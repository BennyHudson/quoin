<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<script src="<?php echo get_stylesheet_directory_uri(); ?>/js/lib/jquery-1.8.2.min.js" type="text/javascript"></script>
<link rel="stylesheet" href="<?php bloginfo('stylesheet_url'); ?>" type="text/css" media="screen" />
<script src="<?php echo get_stylesheet_directory_uri(); ?>/js/lib/modernizr-latest.js" type="text/javascript"></script>
<link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,400,300,700,700italic' rel='stylesheet' type='text/css'>
<meta charset="<?php bloginfo('charset'); ?>">
<meta name="viewport" content="width=device-width"/>  
<?php get_template_part('includes/include', 'favicon'); ?>
<title><?php bloginfo('name'); ?></title>
<?php wp_head(); ?>
<div id="fb-root"></div>
</head>
<body <?php body_class(''); ?>>
	<?php if(!is_front_page()) { ?>
		<header>
			<nav class="alt-nav">
				<section class="container">
					<aside>
						<h2><a href="<?php bloginfo('url'); ?>"><?php bloginfo('name'); ?></a></h2>
					</aside>
					<aside>
						<ul>
							<li><a href="<?php bloginfo('url'); ?>"><i class="fa fa-angle-left"></i> Back Home</a></li>
						</ul>
					</aside>
				</section>
			</nav>
		</header>
	<?php } ?>
