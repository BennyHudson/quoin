<?php get_header(); ?>

	<?php if (have_posts()) : ?>
		<section class="container narrow">
				<?php while ( have_posts() ) : the_post(); ?>
					<h1 class="page-title"><?php the_title(); ?></h1>
					<?php the_post_thumbnail('full-feature'); ?>
					<article>
						<?php the_content(); ?>
					</article>
					<div class="post-meta">
						<p>
							<span class="meta"><strong>Author: </strong> <?php the_author(); ?></span>
							<span class="meta"><strong>Date: </strong> <?php the_time('jS F Y'); ?></span>
							<span class="meta"><strong>Category: </strong><?php the_category(','); ?></span>
						</p>
					</div>
				<?php endwhile; ?>
		</section>
	<?php endif; ?>

<?php get_footer(); ?>
