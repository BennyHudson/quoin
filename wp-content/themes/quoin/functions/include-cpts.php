<?php
	function tastic_posts() {
		register_post_type(
			'health-safety', 
			array(
				'labels'		=> array (
									'name'					=> _x('Health and Safety Documents', 'post type general name'),
									'singular_name'			=> _x('Health and Safety Document', 'post type singular name'),
									'add_new'				=> _x('Add new Health and Safety Document', 'book'),
									'add_new_item'			=> ('Add New Health and Safety Document'),
									'edit_item'				=> ('Edit Health and Safety Document'),
									'new_item'				=> ('New Health and Safety Document'),
									'all_items'				=> ('All Health and Safety Documents'),
									'view_item'				=> ('View Health and Safety Documents'),
									'search_items'			=> ('Search Health and Safety Documents'),
									'not_found'				=> ('No Health and Safety Documents found'),
									'not_found_in_trash'	=> ('No Health and Safety Documents found in trash'),
									'parent_item_colon'		=> '',
									'menu_name'				=> 'Health & Safety '
								),
				'description'	=> 'Holds all the Health and Safety Documents',
				'public'		=> true,
				'menu_position'	=> 19,
				'menu_icon'		=> 'dashicons-hammer',
				'supports'		=> array('title', 'editor', 'excerpt', 'revisions', 'thumbnail'),
				'has_archive'	=> true
			)
		);
	}
	function tastic_taxonomies() {
		
			register_taxonomy( 
				'document-type', 
				'health-safety', 
				array(
		            'labels' 		=> array(
							 			'name'              => _x( 'Document Type', 'taxonomy general name' ),
							            'singular_name'     => _x( 'Document Type', 'taxonomy singular name' ),
							            'search_items'      => __( 'Search Document Types' ),
							            'all_items'         => __( 'All Document Types' ),
							            'parent_item'       => __( 'Parent Document Type' ),
							            'parent_item_colon' => __( 'Parent Document Type:' ),
							            'edit_item'         => __( 'Edit Document Type' ), 
							            'update_item'       => __( 'Update Document Type' ),
							            'add_new_item'      => __( 'Add New Document Type' ),
							            'new_item_name'     => __( 'New Document Type' ),
							            'menu_name'         => __( 'Document Types' ),
							        ),
		            'hierarchical' => true,
		        )
			);
		
    }
?>
