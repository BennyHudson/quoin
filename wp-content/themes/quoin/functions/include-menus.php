<?php
	function register_my_menus() {
	  register_nav_menus(
		array( 'mobile-menu' 	=> __( 'Mobile Menu' ),
			   'footer-menu' 	=> __( 'Footer Menu' ),
			   'header-left' 	=> __( 'Header Left' ),
			   'header-right' 	=> __( 'Header Right' ),
			   'signposts'	 	=> __( 'Signposts' )
			 )
	  );
	}
?>
