<?php
	function tastic_dashboard() {
		global $wp_meta_boxes;
		//wp_add_dashboard_widget('custom_help_widget', 'Welcome!', 'custom_dashboard_help');
		unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_right_now']);
		unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_activity']);
		unset($wp_meta_boxes['dashboard']['normal']['core']['wpseo-dashboard-overview']);
		unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_quick_press']);
		unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_primary']);

		$normal_dashboard = $wp_meta_boxes['dashboard']['normal']['core'];

	 	// Backup and delete our new dashboard widget from the end of the array	 
	 	$example_widget_backup = array( 'custom_help_widget' => $normal_dashboard['custom_help_widget'] );
	 	unset( $normal_dashboard['custom_help_widget'] );

	 	// Merge the two arrays together so our widget is at the beginning	 
	 	$sorted_dashboard = array_merge( $example_widget_backup, $normal_dashboard );

	 	// Save the sorted array back into the original metaboxes 
	 	$wp_meta_boxes['dashboard']['normal']['core'] = $sorted_dashboard;
	}
?>
