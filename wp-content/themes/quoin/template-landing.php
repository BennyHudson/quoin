<?php
	//Template Name: Landing Page
	get_header();
?>

	<section id="feature" data-anchor="feature">
		<video autoplay loop poster="<?php echo get_stylesheet_directory_uri(); ?>/images/poster.jpg" id="bgvid">
			<source src="<?php echo get_stylesheet_directory_uri(); ?>/video/shutterstock_v7465363.webm" type="video/webm">
			<source src="<?php echo get_stylesheet_directory_uri(); ?>/video/shutterstock_v7465363.mp4" type="video/mp4">
		</video>
		<section class="feature-content">
			<section class="container">
				<h1><?php bloginfo('name'); ?></h1>
				<h2>The Quoin Consultancy</h2>
				<ul class="signposts">
					<li><a href="#" data-scroll="about" id="signpost-about"><span></span>About Us</a></li>
					<li><a href="#" data-scroll="cdm" id="signpost-cdm"><span></span>CDM</a></li>
					<li><a href="#" data-scroll="safety" id="signpost-safety"><span></span>Health &amp; Safety</a></li>
					<li><a href="#" data-scroll="news" id="signpost-news"><span></span>News</a></li>
					<li><a href="#" data-scroll="contact" id="signpost-contact"><span></span>Contact</a></li>
				</ul>
				<a href="#" data-scroll="about" class="scroll-trigger"><i class="fa fa-chevron-down fa-2x"></i></a>
			</section>
		</section>
	</section>
	<section class="main-content">
		<nav class="home-nav">
			<section class="container">
				<aside>
					<h2><a href="#" class="scrollToTop"><?php bloginfo('name'); ?></a></h2>
				</aside>
				<aside>
					<ul>
						<li><a href="#" data-scroll="about" id="nav-about">About Us</a></li>
						<li><a href="#" data-scroll="cdm" id="nav-cdm">CDM</a></li>
						<!--<li><a href="#" data-scroll="construction" id="nav-construction">Construction</a></li>-->
						<li><a href="#" data-scroll="safety" id="nav-safety">Health &amp; Safety</a></li>
						<li><a href="#" data-scroll="news" id="nav-news">News</a></li>
						<li><a href="#" data-scroll="contact" id="nav-contact">Contact</a></li>
					</ul>
				</aside>
			</section>
		</nav>
		<section id="about" class="home-section" data-anchor="about">
			<section class="container">
				<h2 class="section-title">About Us</h2>
				<div>
					<aside class="about-feature">
						<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/about-feature.jpg" alt="About Us">
					</aside>
					<aside class="about-content">
						<?php
							$my_id = 8;
							$post_id_8 = get_post($my_id);
							$content = $post_id_8->post_content;
							$content = apply_filters('the_content', $content);
							$content = str_replace(']]>', ']]>', $content);
							echo $content;
						?>
					</aside>
				</div>
			</section>
		</section>
		<section class="home-section parallax" data-parallax="scroll" data-image-src="<?php echo get_stylesheet_directory_uri(); ?>/images/parallax-1.jpg">
			<section class="container">
				<h2 class="parallax-title">Want to find out more about our services?</h2>
				<a href="#" data-scroll="contact" class="scroll-trigger large-button">Contact Us</a>
			</section>
		</section>
		<section id="cdm" class="home-section" data-anchor="cdm">
			<section class="container">
				<h2 class="section-title">CDM Services</h2>
				<?php
					$my_id = 9;
					$post_id_9 = get_post($my_id);
					$content = $post_id_9->post_content;
					$content = apply_filters('the_content', $content);
					$content = str_replace(']]>', ']]>', $content);
					echo $content;
				?>
				<div class="quoin-overlay" id="downloads"></div>
				<div class="overlay-wrap" id="download-content">
					<div class="overlay-container">
						<div class="overlay-content">
							<a href="#" class="overlay-close"><i class="fa fa-times"></i></a>
							<?php if(get_field('cdm_downloads', 9)) { ?>
								<ul>
									<?php while(the_repeater_field('cdm_downloads', 9)) { ?>
										<li><a href="<?php the_sub_field('document', 9); ?>" target="_blank"><i class="fa fa-file-pdf-o"></i> <?php the_sub_field('title', 9); ?></a></li>
									<?php } ?>
								</ul>
							<?php } ?>
						</div>
					</div>
				</div>

				<section class="cdm-content">
					<aside>
						<h2 class="cdm-title" id="what-we-do"><a href="#">What We Provide</a></h2>
						<?php the_field('what_we_provide', 9); ?>
					</aside>
					<aside>
						<h2 class="cdm-title" id="why-quoin"><a href="#">Why Choose Us?</a></h2>
						<?php the_field('why_choose_us', 9); ?>
					</aside>
					<aside>
						<h2 class="cdm-title" id="cdm-downloads"><a href="#">Downloads</a></h2>

					</aside>
				</section>
			</section>
		</section>
		<section id="safety" class="home-section" data-anchor="safety" class="home-section">
			<section class="container">
				<h2 class="section-title">Health &amp; Safety</h2>
				<?php
					$my_id = 68;
					$post_id_68 = get_post($my_id);
					$content = $post_id_68->post_content;
					$content = apply_filters('the_content', $content);
					$content = str_replace(']]>', ']]>', $content);
					echo $content;
				?>
				<ul id="safety-titles">
					<li><a href="" data-slide-index="0">For Workplace</a></li>
					<li><a href="" data-slide-index="1">For Construction</a></li>
				</ul>
				<ul id="safety-content">
					<li>
						<?php 
		                    $args = array(
		                        'post_type'				=> 'health-safety',
		                        'posts_per_page'		=> 8,
		                        'orderby'				=> 'name',
		                        'order'					=> 'asc',
		                        'tax_query' => array(
		                            array(
		                                'taxonomy' => 'document-type',
		                                'field' => 'slug',
		                            	'terms' => 'workplace'
		                        	)
		                    	)
		                    );  
		                    $the_query = new WP_Query( $args );
		                ?>
	                	<?php if($the_query->have_posts()) { ?>
	                		<ul>
	                			<?php while($the_query->have_posts()) { ?>
	                				<li>
	                					<?php $the_query->the_post(); ?>
	                						<a href="<?php the_field('pdf_document'); ?>" target="_blank">
	                							<?php the_post_thumbnail('small-square'); ?>
	                							<span><?php the_title(); ?></span>
	                						</a>
	                					<?php wp_reset_postdata(); ?>
	                				</li>
	                			<?php } ?>
	                		</ul>
	                	<?php } ?>
					</li>
					<li>
						<?php 
		                    $args = array(
		                        'post_type'				=> 'health-safety',
		                        'posts_per_page'		=> 8,
		                        'orderby'				=> 'name',
		                        'order'					=> 'asc',
		                        'tax_query' => array(
		                            array(
		                                'taxonomy' => 'document-type',
		                                'field' => 'slug',
		                            	'terms' => 'construction'
		                        	)
		                    	)
		                    );  
		                    $the_query = new WP_Query( $args );
		                ?>
	                	<?php if($the_query->have_posts()) { ?>
	                		<ul>
	                			<?php while($the_query->have_posts()) { ?>
	                				<li>
	                					<?php $the_query->the_post(); ?>
	                						<a href="<?php the_field('pdf_document'); ?>" target="_blank">
	                							<?php the_post_thumbnail('small-square'); ?>
	                							<span><?php the_title(); ?></span>
	                						</a>
	                					<?php wp_reset_postdata(); ?>
	                				</li>
	                			<?php } ?>
	                		</ul>
	                	<?php } ?>
					</li>
				</ul>	
			</section>
		</section>
		<?php 
			$quoinposts = get_posts('numberposts=1');
			if($quoinposts) {
		?>
			<section class="home-section parallax" data-parallax="scroll" data-image-src="<?php echo get_stylesheet_directory_uri(); ?>/images/parallax-2.jpg">
				<section class="container">
					<h2 class="parallax-title">We help make your vision a Reality</h2>
					<a href="#" data-scroll="contact" class="scroll-trigger large-button">Contact Us</a>
				</section>
			</section>
			<section id="news" class="home-section" data-anchor="news">
				<section class="container">
					<h2 class="section-title">Latest News</h2>
					<section class="news-archive">		
						<?php 
						$postslist = get_posts('numberposts=1');
		                foreach ($postslist as $post) { ?>
		                	<?php setup_postdata($post); ?>
			            		<aside class="large-feature">
			            			<a href="<?php the_permalink(); ?>">
			            				<div class="overlay">
			            					<h2><?php the_title(); ?></h2>
			            				</div>
			            				<?php the_post_thumbnail('feature-square'); ?>
			            			</a>
			            		</aside>
		            		<?php wp_reset_postdata(); ?>
		            	<?php } ?>
		            	<aside class="news-stack">
			            	<?php 
							$postslist = get_posts('numberposts=3&offset=1');
			                foreach ($postslist as $post) { ?>
			                	<?php setup_postdata($post); ?>
			            			<a href="<?php the_permalink(); ?>">
			            				<div class="overlay">
			            					<h2><?php the_title(); ?></h2>
			            				</div>
			            				<?php the_post_thumbnail('skinny-feature'); ?>
			            			</a>
			            		<?php wp_reset_postdata(); ?>
			            	<?php } ?>
		            	</aside>
	            	</section>

				</section>
			</section>
		<?php } ?>
		<section id="contact" class="home-section parallax" data-anchor="contact" data-parallax="scroll" data-image-src="<?php echo get_stylesheet_directory_uri(); ?>/images/large-city-alt.jpg">
			<section class="container">
				<h2 class="section-title">Contact</h2>
				<div class="contact-area">
					<aside class="form">
						<?php echo do_shortcode('[contact-form-7 id="23" title="Contact Quoin"]'); ?>
					</aside>
					<aside class="contact-details">
						<ul>
							<li>
								<aside class="icon"><i class="fa fa-map-marker"></i></aside>
								<aside class="content double">1 Sandy Lane, Little Sandhurst, <br />Berkshire, GU47 8NL</aside>
							</li>
							<li>
								<aside class="icon"><i class="fa fa-envelope"></i></aside>
								<aside class="content"><a href="mailto:marketing@quoin.co.uk">marketing@quoin.co.uk</a></aside>
							</li>
							<li>
								<aside class="icon"><i class="fa fa-phone"></i></aside>
								<aside class="content">01344 89 33 89</aside>
							</li>
						</ul>
					</aside>
				</div>
			</section>
		</section>
	</section>

<?php get_footer(); ?>
