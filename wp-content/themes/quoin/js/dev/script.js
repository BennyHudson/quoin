$(document).ready(function() {

    $('nav ul a, a.scroll-trigger, ul.signposts a').click(function(e) {
        var navHeight = $('nav.home-nav').outerHeight();
        var scrollAnchor = $(this).attr('data-scroll'),
            scrollPoint = $('section#' + scrollAnchor).offset().top - navHeight;
        e.preventDefault();
        $('body,html').animate({
            scrollTop: scrollPoint
        }, 1000);
        return false;
    });

    $('.scrollToTop').click(function(){
        $('html, body').animate({scrollTop : 0},800);
        return false;
    });

    jQuery('.parallax').parallax();

    var featureHeight = $('.large-feature').outerHeight();
    var stripeHeight = featureHeight/3;

    $('.news-stack > a').css('height', stripeHeight);

    jQuery('#home-slider').bxSlider({
        pager: false,
        nextText: '<i class="fa fa-angle-right"></i>',
        prevText: '<i class="fa fa-angle-left"></i>'
    });

    jQuery('#safety-content').bxSlider({
        controls: false,
        pagerCustom: '#safety-titles'
    });

    $('#cdm-downloads a').click(function(e) {
        e.preventDefault();
        $('html').css('overflow', 'hidden');
        $('#downloads, #download-content').show();
    });

    $('.overlay-close').click(function(e) {
        e.preventDefault();
        $('html').css('overflow', 'auto');
        $('.quoin-overlay, .overlay-wrap').hide();
    });

});
$(window).scroll(function() {
    var featureHeight = $('#feature').outerHeight();
    var navHeight = $('nav.home-nav').outerHeight();
    if ($(this).scrollTop() > (featureHeight - 1)) {
        $('nav.home-nav').addClass('fixed-nav');
        $('video').hide();
        $('.home-section#about').css('margin-top', navHeight);
    } else {
        $('nav.home-nav').removeClass('fixed-nav');
        $('video').show();
        $('.home-section#about').css('margin-top', 0);
    }
});
